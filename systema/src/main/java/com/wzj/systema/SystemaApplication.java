package com.wzj.systema;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.stereotype.Repository;

@MapperScan(basePackages = {"com.wzj"}, annotationClass = Repository.class)
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SystemaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemaApplication.class, args);
	}

}
