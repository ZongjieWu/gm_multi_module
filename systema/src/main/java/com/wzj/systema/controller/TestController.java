package com.wzj.systema.controller;

import com.wzj.systema.entity.User;
import com.wzj.systema.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Zongjie Wu
 * @date 2021/3/22 15:39
 */
@RequestMapping("test")
@RestController
public class TestController {

    @Autowired
    private UserService userService;
    /**
     * 测试程序
     * @return
     */
    @GetMapping(value = "test")
    public Map<String,Object> test(){
        Map<String,Object> result=new HashMap<>();
        User user=userService.getById(1L);
        result.put("code",0);
        result.put("msg","success");
        result.put("data",user);
        return result;
    }
}
