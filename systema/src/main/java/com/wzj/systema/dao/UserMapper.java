package com.wzj.systema.dao;

import com.wzj.systema.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    /**
     *
     * @mbg.generated 2021-03-22
     */
    int deleteByPrimaryKey(Long id);

    /**
     *
     * @mbg.generated 2021-03-22
     */
    int insert(User record);

    /**
     *
     * @mbg.generated 2021-03-22
     */
    int insertSelective(User record);

    /**
     *
     * @mbg.generated 2021-03-22
     */
    User selectByPrimaryKey(Long id);

    /**
     *
     * @mbg.generated 2021-03-22
     */
    int updateByPrimaryKeySelective(User record);

    /**
     *
     * @mbg.generated 2021-03-22
     */
    int updateByPrimaryKey(User record);
}