package com.wzj.systema.service.impl;

import com.wzj.systema.dao.UserMapper;
import com.wzj.systema.entity.User;
import com.wzj.systema.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Zongjie Wu
 * @date 2021/3/22 16:50
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public User getById(Long id) {
        return userMapper.selectByPrimaryKey(id);
    }
}
