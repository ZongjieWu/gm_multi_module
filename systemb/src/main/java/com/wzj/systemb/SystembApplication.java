package com.wzj.systemb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Repository;

@MapperScan(basePackages = {"com.wzj"}, annotationClass = Repository.class)
@SpringBootApplication
public class SystembApplication {
	public static void main(String[] args) {
		SpringApplication.run(SystembApplication.class, args);
	}

}
