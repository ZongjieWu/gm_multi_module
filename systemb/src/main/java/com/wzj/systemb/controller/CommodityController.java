package com.wzj.systemb.controller;


import com.wzj.systemb.entity.Commodity;
import com.wzj.systemb.service.ICommodityService;
import org.omg.CORBA.MARSHAL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
@RestController
@RequestMapping("/commodity")
public class CommodityController {

    @Autowired
    private ICommodityService commodityService;

    /**
     * 根据id获取商品信息
     * @return
     */
    @GetMapping(value = "getById")
    public Map<String,Object> getById(Long id){
        Commodity commodity=commodityService.getById(id);
        Map<String,Object> map=new HashMap<>();
        map.put("code",0);
        map.put("msg","success");
        map.put("data",commodity);
        return map;
    }
}

