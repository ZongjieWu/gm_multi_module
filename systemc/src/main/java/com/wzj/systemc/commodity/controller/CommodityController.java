package com.wzj.systemc.commodity.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wzj.systemc.commodity.entity.Commodity;
import com.wzj.systemc.commodity.service.ICommodityService;
import com.wzj.systemc.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  商品相关
 * @author ZongjieWu
 * @since 2021-03-31
 */
@RestController
@RequestMapping("/commodity")
public class CommodityController {

    @Autowired
    private ICommodityService commodityService;
    /**
     * 根据用id获取信息
     * @param id id
     * @return
     */
    @GetMapping(value = "getById")
    public Map<String,Object> getById(Long id){
        Map<String,Object> map=new HashMap<>();
        QueryWrapper<Commodity> queryWrapper = new QueryWrapper<Commodity>();
        queryWrapper.lambda().eq(Commodity::getId, id);
        List<Commodity> commodity =commodityService.list(queryWrapper);
        map.put("code",0);
        map.put("msg","success");
        map.put("data", commodity);
        return map;
    }
}

