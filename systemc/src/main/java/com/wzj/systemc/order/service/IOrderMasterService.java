package com.wzj.systemc.order.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wzj.systemc.order.entity.OrderMaster;

import java.util.Map;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
public interface IOrderMasterService extends IService<OrderMaster> {
    /**
     * 根据id查询详情
     * @param userId
     * @return
     */
    Map<String,Object> detailById(Long userId);
}
