package com.wzj.systemc.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wzj.systemc.order.dao.OrderMasterMapper;
import com.wzj.systemc.order.dto.OrderMasterDataDTO;
import com.wzj.systemc.order.entity.OrderMaster;
import com.wzj.systemc.order.service.IOrderMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
@Service
public class OrderMasterServiceImpl extends ServiceImpl<OrderMasterMapper, OrderMaster> implements IOrderMasterService {

    @Autowired
    private OrderMasterMapper orderMasterMapper;

    @Override
    public Map<String, Object> detailById(Long userId) {
        OrderMasterDataDTO orderMasterDataDTO = orderMasterMapper.detailById(userId);
        Map<String,Object> map=new HashMap<>();
        map.put("code",0);
        map.put("msg","success");
        map.put("data",orderMasterDataDTO);
        return map;
    }
}
