package com.wzj.systemc.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wzj.systemc.order.entity.OrderMaster;
import com.wzj.systemc.user.entity.User;
import com.wzj.systemc.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  用户相关接口
 * @author ZongjieWu
 * @since 2021-03-31
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;
    /**
     * 根据用户id获取用户信息
     * @param id) 用户id
     * @return
     */
    @GetMapping(value = "getByUserId")
    public Map<String,Object> getByUserId(Long id){
        Map<String,Object> map=new HashMap<>();
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.lambda().eq(User::getId, id);
        List<User> userList =userService.list(queryWrapper);
        map.put("code",0);
        map.put("msg","success");
        map.put("data", userList);
        return map;
    }
}

