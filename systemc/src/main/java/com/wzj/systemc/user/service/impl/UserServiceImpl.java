package com.wzj.systemc.user.service.impl;

import com.wzj.systemc.user.entity.User;
import com.wzj.systemc.user.dao.UserMapper;
import com.wzj.systemc.user.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-31
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
